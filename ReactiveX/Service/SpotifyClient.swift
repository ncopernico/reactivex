//
//  SpotifyClient.swift
//  RxSpotifySearch
//
//  Created by Adam Borek on 04/11/2016.
//  Copyright © 2016 Adam Borek. All rights reserved.
//

import Foundation

class SpotifyClient {
    
    let session = URLSession(configuration: URLSessionConfiguration.default)
    
    func printToken() {
        
        if let url = URL(string: "https://accounts.spotify.com/api/token") {
            var postRequest = URLRequest(url: url)
            postRequest.httpMethod = "POST"
            let bodyParams = "grant_type=client_credentials"
            postRequest.httpBody = bodyParams.data(using: String.Encoding.ascii, allowLossyConversion: true)
            
            let id = "efed8c80ba744084bbe4179a7a2510d7"
            let secret = "5f84ba5f5b404fc68d1732e9b2af989d"
            let combo = "\(id):\(secret)".toBase64()
            postRequest.addValue("Basic \(combo)", forHTTPHeaderField: "Authorization")
            
            print(postRequest.cURL)
                        
            let task = URLSession.shared.dataTask(with: postRequest) { (data, response, error) in
                guard let data = data else {
                    return
                }
                print(String(data: data, encoding: String.Encoding.utf8)!)
                
            }
            task.resume()
        }

    }
    
    func request(query: String) -> URLRequest {
        let encodedQuery = encode(query: query) ?? ""
        var request = URLRequest(url: URL(string: "https://api.spotify.com/v1/search?q=\(encodedQuery)&type=track&market=US")!)
        request.httpMethod = "GET"
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("Bearer BQDl19DWLkg2C20ytSbZEpRzMfj12DQXL0AjPAqSC4r53szTGUBLKxGiOqaYxSqVHERyUf9F_x7ihvzLSTI", forHTTPHeaderField: "Authorization")

        return request
    }
    
    private func encode(query: String) -> String? {
        var allowedCharacterSet = CharacterSet.urlQueryAllowed
        allowedCharacterSet.insert(charactersIn: " ")
        return query.addingPercentEncoding(withAllowedCharacters: allowedCharacterSet)?
            .replacingOccurrences(of: " ", with: "+")
    }
    
}


extension String {
    
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self) else {
            return nil
        }
        
        return String(data: data, encoding: .utf8)
    }
    
    func toBase64() -> String {
        return Data(self.utf8).base64EncodedString()
    }
}

extension URLRequest {
    
    /// Returns a cURL command for a request
    /// - return A String object that contains cURL command or "" if an URL is not properly initalized.
    public var cURL: String {
        
        guard
            let url = url,
            let httpMethod = httpMethod,
            url.absoluteString.utf8.count > 0
            else {
                return ""
        }
        
        var curlCommand = "curl --verbose \\\n"
        
        // URL
        curlCommand = curlCommand.appendingFormat(" '%@' \\\n", url.absoluteString)
        
        // Method if different from GET
        if "GET" != httpMethod {
            curlCommand = curlCommand.appendingFormat(" -X %@ \\\n", httpMethod)
        }
        
        // Headers
        let allHeadersFields = allHTTPHeaderFields!
        let allHeadersKeys = Array(allHeadersFields.keys)
        let sortedHeadersKeys  = allHeadersKeys.sorted(by: <)
        for key in sortedHeadersKeys {
            curlCommand = curlCommand.appendingFormat(" -H '%@: %@' \\\n", key, self.value(forHTTPHeaderField: key)!)
        }
        
        // HTTP body
        if let httpBody = httpBody, httpBody.count > 0 {
            let httpBodyString = String(data: httpBody, encoding: String.Encoding.utf8)!
            let escapedHttpBody = URLRequest.escapeAllSingleQuotes(httpBodyString)
            curlCommand = curlCommand.appendingFormat(" --data '%@' \\\n", escapedHttpBody)
        }
        
        return curlCommand
    }
    
    /// Escapes all single quotes for shell from a given string.
    static func escapeAllSingleQuotes(_ value: String) -> String {
        return value.replacingOccurrences(of: "'", with: "'\\''")
    }
}
