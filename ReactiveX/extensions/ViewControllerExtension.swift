//
//  ViewControllerExtension.swift
//  ReactiveX
//
//  Created by Marcos Gonzalez Sierra on 7/10/18.
//  Copyright © 2018 Marcos Gonzalez Sierra. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

extension ViewController {
    
    func configure() {
        
        // Observable de cada vez que hacer pull to refresh
        let didPullToRefresh: Observable<Bool> = refreshControl.rx.refresing.asObservable()
        
        // Accedemos a la extension reactiva del searchBar
        let textTyped = searchController.searchBar.rx.text.orEmpty
            .skip(1)
            .distinctUntilChanged()
        
        let textToQuery = textTyped // Observable<String>
            .debounce(0.3, scheduler: MainScheduler.instance) // Observable<String>
            .startWith("The Wall") // Observable<String>
            .filter { text in
                return text.count > 2
        }
        
        let textForPull = didPullToRefresh // Observable<String>
            .withLatestFrom(textToQuery) {  _, text in
                return text
        }
        
        let tracksSpotify = Observable.merge(textToQuery, textForPull)// Observable<String>
            .flatMapLatest { [spotyfyPresent] textToQuery in // Observable<[Track]>
                return spotyfyPresent.tracks(withText: textToQuery)
                    .retry(3)
                    .catchErrorJustReturn([Track]()) // Observable<[Track]>
            }
            .do( onNext: { [weak self] _ in
                self?.refreshControl.endRefreshing()
            })
        
        let clearTracks = textTyped
            .map { _ in // Observable<[Track]>
                return [Track]()
        }
        
        let tracksAndClear = Observable.merge(tracksSpotify, clearTracks)
        
        let trackRenderables = tracksAndClear
            .map { tracks in // Observable<[TrackRenderable]>
                return tracks.map(TrackRenderable.init)
        }
        
        trackRenderables
            .bind(to: tableView.rx.items(cellIdentifier: "TrackCell", cellType: TrackCell.self)) { index, track, cell in
                cell.render(trackRenderable: track)
            }
            .disposed(by: bag)

    }
}
