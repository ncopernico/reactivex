//
//  SpotifyClientExtension.swift
//  ReactiveX
//
//  Created by Marcos Gonzalez Sierra on 4/11/18.
//  Copyright © 2018 Marcos Gonzalez Sierra. All rights reserved.
//

import Foundation
import RxSwift
import SwiftyJSON

extension SpotifyClient {
    
    func tracks(withText text: String) -> Observable<[Track]> {
        
        print("--- Request tracks \(text)")
        
        return Observable.create { observer in
            
            let request = self.request(query: text)
            let task = self.session.dataTask(with: request) { data, response, error in
                
                if let error = error {
                    print(error.localizedDescription)
                    observer.onError(error)
                }
                
                let tracks = data.flatMap { try? JSON(data: $0) }
                    .map(self.parseTracks) ?? []
                observer.onNext(tracks)
                observer.onCompleted()
            }
            
            task.resume()
            
            return Disposables.create {
                
                task.cancel()
            }
            }.observeOn(MainScheduler.instance)
    }
    
    private func parseTracks(json: JSON) -> [Track] {
        
        return Track.tracks(json: json["tracks"]["items"].array)
    }
}
