//
//  RefreshControlReactiveExtension.swift
//  ReactiveX
//
//  Created by Marcos Gonzalez Sierra on 4/11/18.
//  Copyright © 2018 Marcos Gonzalez Sierra. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

extension Reactive where Base: UIRefreshControl {

    public var refresing: ControlProperty<Bool> {
        
        let source: Observable<Bool> = Observable.deferred { [weak refreshControl = self.base as UIRefreshControl] () -> Observable<Bool> in
            
            guard let refreshControl = refreshControl  else {
                return Observable.just(false)
            }
            
            return  refreshControl.rx.controlEvent(.valueChanged)
                .map { [refreshControl] in
                    return refreshControl.isRefreshing
                }
                .filter { $0 == true }

        }
        
        let bindingObserver: Binder<Bool> = Binder(self.base) { refreshControl, refresh in
            if refresh {
                refreshControl.beginRefreshing()
            } else {
                refreshControl.endRefreshing()
            }
        }
        
        return ControlProperty(values: source, valueSink: bindingObserver)

    }
}

