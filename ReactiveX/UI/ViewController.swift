//
//  ViewController.swift
//  ReactiveX
//
//  Created by Marcos Gonzalez Sierra on 6/10/18.
//  Copyright © 2018 Marcos Gonzalez Sierra. All rights reserved.
//

import UIKit
import RxSwift

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    let spotyfyPresent = SpotifyClient()
    let bag = DisposeBag()
    
    let refreshControl = UIRefreshControl()
    let searchController: UISearchController = {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchBar.placeholder = "Search for tracks"
        return searchController
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        definesPresentationContext = true
        
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        
        tableView.addSubview(refreshControl)

        configure()
//        spotyfyPresent.printToken()
    }
    
}


